from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy import log

START_PAGE_NUMBER = 1
END_PAGE_NUMBER = 3
PAGINATION_URL = None#'/page:{page}'

class TutorialSpider(BaseSpider):
    rate = 1
    name = "tutorial"
    allowed_domains = ["*"]
    start_urls = [
        "http://www.redhotcruiseplanners.com/searches/cruise",
    ]
    def __init__(self):
        self.download_delay = 10/float(self.rate)
        start_url = self.start_urls[0]
        if PAGINATION_URL != None:
            if(PAGINATION_URL.find('{page}') == -1):
                log.msg('Pagination configuration is wrong, missing string "{page}"', log.WARNING)
            else:
                for page in range(START_PAGE_NUMBER, END_PAGE_NUMBER):
                    append_str = PAGINATION_URL
                    if append_str[0] != '/' and start_url[-1] != '/':
                        append_str = '/' + append_str
                    url = append_str.format(page=page)
                    self.start_urls.append(start_url + url)

    def parse(self, response):
        filename = response.url.split("/")[-2]
        hxs = HtmlXPathSelector(response)
        items = hxs.select("//div[@class='search_result mobile-top-container bottom-line']")

        for i in items:
            cruise_name = i.select("div/table/tbody/tr/td/h3[@style='padding: 0; margin: 0']/a/text()").extract()
            ship_name = i.select("div/table/tbody/tr[position()=2]/td[position()=2]/div/a[position()=2]/text()").extract()
            cruise_line = i.select("div/table/tbody/tr[position()=2]/td[position()=2]/div/a[position()=1]/text()").extract()
            description = i.select("div/table/tbody/tr[position()=3]/td/div[position()=2]/p").extract()
            description_temp = ''
            for d in description:
                description_temp += d
        open(filename, 'wb').write(str(description_temp))